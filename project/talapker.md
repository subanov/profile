---
layout: page
title: Election Candidates Information Site
permalink: /project/talapker/
---

the Central Election Commission (CEC) required to publish election candidates information.
As a lead technical consultant, I was in charge of gathering requirements and implementing the project.

### Tech Stack

|Django|Provides business logic and rest api in json|
|PostgreSQL|RDBMS|