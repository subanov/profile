---
layout: page
title: New Election Statistics System
permalink: /project/newess/
---

The Central Election Commission (CEC) already had an infromation system that counted votes.

There were two major problems that we need to solve:
- Performance issues
- Better UI, easy to be understood

Old application was running in korean Jeus Application server. Altibase, korean database, used as RDBMS.
As a lead technical consultant, I was in charge of gathering requirements and implementing the project.

### Tech Stack

|Spring Boot|Provides business logic and rest api in json|
|JdbcTemplate|We found only way to connect Altibase is thru JDBC|
|React|Client side|