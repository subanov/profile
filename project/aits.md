---
layout: page
title: Animal Identification and Traceability System
permalink: /project/aits/
---

## Objective

Identifying and tracing animals plays a major role in food security.It was decided to create a mobile application that is used by veterinarians.

### Components

- Web application
    - Manage system information
    - Reporting
    - Security
- Public site to check
    - Animal movements
    - Animal health status
- Mobile application
    - Enter animal data
    - Print movement/health cheques

### Tech Stack

|Asp.Net Web Api|Provides business logic and rest api in json|
|React Native|android application|
|PostgreSQL|RDMS|
|Angular|Client side|