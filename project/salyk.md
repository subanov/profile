---
layout: page
title: Taxpayer's Personnal Area/Cabinet
permalink: /project/salyk/
---

Taxpayers require generated forms to submit their reports.
Our system generates the necessary forms based on uploaded Excel sheets.
Excell sheets could contain form rules as well. Ex: Field3 = Field1 + Field2

### Tech Stack

|Asp.Net MVC|Business logic, main component|
|MS SQL|RDMS|
|HTML/CSS, JQuery|Client side|