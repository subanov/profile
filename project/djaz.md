---
layout: page
title: Djaz - electronic signature solution that focuses on document flow [rather than document]
permalink: /project/djaz/
---

Project main components:
- Web application
- OAuth2 service
- Site

## Web application

Sign, send and collect document. Stores document / template files.
All private files are encrypted.

## OAuth2 service

Mainly provides tokens to web, mobile application and to administrative sites.
Stores user data.

## Site

Provides content. Interface to search templates and provide feedback.

### Tech Stack

|Asp.Net Web Api|Provides business logic and rest api in json|
|Elastic Stack|Used for full text search and logging|
|PostgreSQL|RDMS|
|Django|Additionally djangocms and djangocms-blogging were used for content|
|Angular|Client side|