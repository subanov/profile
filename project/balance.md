---
layout: page
title: Mobile Payment System balance.kg
permalink: /project/balance/
---

Beeline outsourced developent of balance.kg and was trying to move to in-house development.
As lead developer, prepared the engineering team to understand and work with the product.

### Tech Stack

|JEE (Spring)|Api|
|Apache Kafka|Messaging|
|PostgreSQL|RDMS|
|Native iOS & android|Mobile|